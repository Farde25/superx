/* number of vowels in string*/

import java.io.*;

 class Program5{
    public static void main(String[] args)throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter String: ");
        String str = br.readLine();
        char[] ch = str.toCharArray();
        int count1 = 0;
        int count2 = 0;
        int count3 = 0;
        int count4 = 0;
        int count5 = 0;

        for(int i = 0;i<ch.length;i++){
            if(ch[i] == 'a'){
                count1++;
            }else if(ch[i] == 'e'){
                count2++;
            }else if(ch[i] == 'i'){
                count3++;
            }else if(ch[i] == 'o'){
                count4++;
            }else if(ch[i] == 'u'){
                count4++;
            }
        }
        System.out.println("a = "+count1);
        System.out.println("e = "+count2);
        System.out.println("i = "+count3);
        System.out.println("o = "+count4);
        System.out.println("u = "+count4);
        
    }
 }