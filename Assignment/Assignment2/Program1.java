public class Program1 {
    public static void main(String[] args) {
        int num = 10;

        int temp = num;
        int sum = 0;
        while(temp != 0){
            int rem = temp%10;
            sum = sum*10+rem;
            temp = temp/10;
        }


        if(num < 0){
            sum = -(sum);
            System.out.println(sum+"-"+" is not palindrome");
        }else if(num != sum){
            System.out.println(sum+" is not palindrome");
        }else{
            System.out.println(num+" is Palindrome");
        }
    }
}
