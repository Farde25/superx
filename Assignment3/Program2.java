/*1 
  1 2 
  2 3 4
  4 5 6 7*/
import java.io.*;
 class Program2{
    public static void main(String[] args)throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int row = Integer.parseInt(br.readLine());
        int n1 = 1;
        for(int i = 1; i<=row;i++){                                
            for(int j = 1;j<=i;j++){
                System.out.print(n1+" ");
                n1++;
            }System.out.println();
            n1--;
        }
    }
 }