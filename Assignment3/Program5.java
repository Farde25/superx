/*String count*/

import java.io.*;

 class Program5{
    public static void main(String[] args)throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter String: ");
        String str = br.readLine();
        int count = 0;
        char[] str1 = str.toCharArray();
        for(int i = 0;i<str1.length;i++){
            count++;
        }
        System.out.println(count);
        
    }
 }