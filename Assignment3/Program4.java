/*Odd number in range */

import java.io.*;

 class Program4{
    public static void main(String[] args)throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter starting number : ");
        int n1 = Integer.parseInt(br.readLine());
        System.out.println("Enter Ending number : ");
        int n2 = Integer.parseInt(br.readLine());
        for(int i = n1;i<=n2;i++){
            if(i %2 != 0){
                System.out.println(i+" is odd number");
            }
        }
        
    }
 }