class Program5{
    public static void main(String[] args){
        String s = "Sa@ns&kr-uti";
        char  ch[] = s.toCharArray();
        System.out.print("String contains following letters : ");
        for(int i = 0;i<ch.length;i++){
            if((ch[i] >= 65 && ch[i] <= 90)||(ch[i] >= 97 && ch[i] <= 122)){
                continue;
            }else{
                System.out.print(ch[i]+" ");
            }
        }
        System.out.println();
    }
}