/*1
  2 4
  3 6 9
  4 8 12 16*/

  import java.io.*;
 class Program2{
    public static void main(String[] args)throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter number of rows : ");
        int n = Integer.parseInt(br.readLine());

        int num = 1;

        for(int i = 1;i<=n;i++){
            for(int j = 1;j<=i;j++){
                System.out.print(num*j+" ");
            }System.out.println();
            num++;
        }
    }
}
 
