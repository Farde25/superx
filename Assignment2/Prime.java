import java.io.*;
class PrimeDemo{
    static void Prime(int n){
        int count=0;
        for(int i = 1;i<=n;i++){
            if(n%i == 0){
                count++;
            }
        }
        if(count == 2){
            System.out.println(n+ " is Prime Number");
        }
    }
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        int n1 = Integer.parseInt(br.readLine());
        int n2 = Integer.parseInt(br.readLine());

        for(int i = n1;i<=n2;i++){
            Prime(i);
        }
    }
}