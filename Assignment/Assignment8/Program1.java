class program1{
     static int small(int []arr){
        int min = Integer.MAX_VALUE;
        int index = 0;
        for(int i = 0;i<arr.length;i++){
            if(i%10==0){
                index= i;
            }
            if(min > i){
                min = i;
            }
        }
        if(min >= 0){
            return min;
        }else{
            return -1;
        }
     }
     public static void main(String[] args) {
        int arr[] = new int[]{1,2,3,2};
        System.out.println(small(arr));
     }
}