/* Prime or composite number in given range*/
import java.io.*;
class Program4{
    static void Prime(int n){
        int count=0;
        for(int i = 1;i<=n;i++){
            if(n%i == 0){
                count++;
            }
        }
        if(count == 2){
            System.out.println(n+ " is Prime Number");
        }else{
            System.out.println(n+ " is Composite Number");
        }
    }
    public static void main(String[] args) throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter 1st number : ");
        int n1 = Integer.parseInt(br.readLine());
        System.out.println("Enter 2nd number : ");
        int n2 = Integer.parseInt(br.readLine());

        for(int i = n1;i<=n2;i++){
            Prime(i);
        }
    }
}