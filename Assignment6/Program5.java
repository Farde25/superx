class Program5{
    public static void main(String[] args){
        String s = "Java";
        char  ch[] = s.toCharArray();
        System.out.print("String contains following letters : ");
        for(int i = 0;i<ch.length;i++){
            if((ch[i] >= 65 && ch[i] <= 90)){
                System.out.print((char)(ch[i]+32));
            }else if(ch[i] >= 97 && ch[i] <= 122){
                System.out.print((char)(ch[i]-32));
            }
        }
        System.out.println();
    }
}