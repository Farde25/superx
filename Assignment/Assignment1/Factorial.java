
class FactDemo{
    static void Factorial(int num){
        int num1 = 0;
        //reverse
        while(num != 0){
            int rem = num%10;
            num1 = num1*10+rem;
            num = num/10;
        } 
        while(num1 != 0){
            int rem = num1%10;
            int mul = 1;
            if(rem % 2 == 0){ // even number
                for(int i = 1;i<=rem;i++){ //factor of even number
                    mul = mul*i;
                }
                System.out.print(mul+" , ");
            }
            
            num1 = num1/10;
        }
    }

    public static void main(String[] args){
        int n = 256946;
        Factorial(n);
        
    }
}