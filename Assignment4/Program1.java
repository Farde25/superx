/*A B C D 
  B C D E
  C D E F
  D E F G*/

  import java.io.*;
 class Program1{
    public static void main(String[] args)throws IOException{
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter number of rows : ");
        int n = Integer.parseInt(br.readLine());
        char ch = 'A';
        char ch1 = ch;
        for(int i = 1;i<=n;i++){
            ch = ch1;
            for(int j = 1;j<=n;j++){
                System.out.print(ch+" ");
                ch++;
            }ch1++;
            System.out.println();
        }
    }
 }
