class Program3{
    public static void main(String[] args){
        int num = 145;
        int temp = num;
        int sum = 0;
        while(num != 0){
            int rem = num%10;
            int mul = 1;
            for(int i = 1;i<=rem;i++){
                mul = mul*i;
            }
            sum = sum + mul;
            num = num/10;
        }
        if(temp == sum){
            System.out.println(temp + "  is strong number");
        }else{
            System.out.println(temp + "  is not strong number");
        }
    }
}