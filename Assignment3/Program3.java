/*Odd number */

import java.io.*;

 class Program3{
    public static void main(String[] args)throws IOException{

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Enter the number : ");
        int n = Integer.parseInt(br.readLine());
        if(n %2 !=0){
            System.out.println(n+" is odd number");
        }else{
            System.out.println(n+" is not odd number");
        }
    }
 }