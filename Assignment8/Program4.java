public class Program4 {
    public static void main(String[] args) {
        int num = 145;
        int temp = num;
        int sum = 0;

        while(temp != 0){
            int rem = temp%10;
            int mul = 1;
            for(int i = 1;i<=rem;i++){
                mul = mul*i;
            }
            sum = sum+mul;
            temp = temp/10;
        }
        if(num == sum){
            System.out.println(num+" is strong number");
        }else{
            System.out.println(num+" is not strong number");
        }
    }
}
